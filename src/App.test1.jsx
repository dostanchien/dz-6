import React from "react";
import {render} from '@testing-library/react'
import App from './App'
import store from "./store";
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import { describe, expect, it } from 'vitest';

describe('App', () => {
	it("snapshot APP", () => {
		const app = render(<Provider store={store}>
			<BrowserRouter>
				<App/>
			</BrowserRouter>
		</Provider>)
		expect(app).toMatchSnapshot()
	})
})
