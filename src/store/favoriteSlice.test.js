import favoriteSlice,{actionModal,actionFavorite,actionCurrentPost} from './favoriteSlice.js';
import { describe, expect, it } from 'vitest';

describe("favoriteSlice", () => {
	it("initial state", () => {
		expect(favoriteSlice(undefined, {type:undefined})).toEqual({
			favorites:{},
			currentPost: {},
			isModal: false
		})
	})

	it("actionModal", () => {
		const previousState = {
			isModal: false
		}
		expect(favoriteSlice(previousState, actionModal())).toEqual({
			isModal: true,
		})
	})

	it("actionAddCurrentPost", () => {
		const previousState = {
			currentPost:{}
		}
		expect(favoriteSlice(previousState, actionCurrentPost({"article":"01"}))).toEqual({
			currentPost: {"article":"01"},
		})
	})

	it("actionFavorites", () => {
		const previousState = {
			favorites:
				{['1']:1,['2']:1}
			
		}
		expect(favoriteSlice(previousState, actionFavorite({'article':5}))).toEqual({
			favorites: 
				{['1']:1,['2']:1,['5']:1}
			,
		})
	})
})


