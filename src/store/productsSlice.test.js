import productsSlice,{actionAddToProducts,actionCurrentPost,actionLoading} from './productsSlice.js';
import { describe, expect, it } from 'vitest';

describe("productsSlice", () => {
	it("initial state", () => {
		expect(productsSlice(undefined, {type:undefined})).toEqual({
			products: [],
			currentPost: {},
			isLoading: true
		})
	})
    it("actionLoading", () => {
		const previousState = {
			isLoading:  true,
		}
		expect(productsSlice(previousState, actionLoading({"article":"01"}))).toEqual({
			isLoading:  {"article":"01"},
		})
	})
	
	it("actionAddCurrentPost", () => {
		const previousState = {
			currentPost:{}
		}
		expect(productsSlice(previousState, actionCurrentPost({"article":"01"}))).toEqual({
			currentPost: {"article":"01"},
		})
	})

	it("actionAddToProducts", () => {
		const previousState = {
			products: [],
		}
		expect(productsSlice(previousState, actionAddToProducts([{"article":"01"}]))).toEqual({
			products: [{"article":"01"}],
		})
	})
})


