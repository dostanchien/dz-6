import {configureStore} from "@reduxjs/toolkit";
import productsReducer from "./productsSlice";
import favoriteReducer from "./favoriteSlice";
import orderReducer from "./orderSlice";

export default configureStore({
    reducer: {
        product: productsReducer,
        favorite: favoriteReducer,
        order: orderReducer,
    },
})