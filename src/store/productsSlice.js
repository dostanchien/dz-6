import {createSlice} from "@reduxjs/toolkit";
import { sendRequest } from "../helpers/sendRequest.js"

const productsSlice = createSlice({
    name: "product",
    initialState: {
       products: [],
       currentPost: {},
       isLoading: true
    },
    reducers: {
        actionAddToProducts: (state, {payload}) => {
            state.products = [...payload]
        },
		actionCurrentPost: (state,{payload}) => {
			state.currentPost = {...payload}
		},
        actionLoading: (state,{payload}) => {
            state.isLoading = payload
        }
    }
})

export const {actionAddToProducts,actionLoading,actionCurrentPost} = productsSlice.actions;

export const actionFetchProducts = () => (dispatch) => {
    dispatch(actionLoading(true));
    return sendRequest(window.location.origin +'/products.json')
    .then((data) => { 
        dispatch(actionAddToProducts(data));
        dispatch(actionLoading(false));
    })
}
export default productsSlice.reducer;