import orderSlice,{actionOrderIsModal,actionOrder,actionCurrentPost,actionPlus,actionMinus,actionLoading} from './orderSlice.js';
import { describe, expect, it } from 'vitest';

describe("orderSlice", () => {
	it("initial state", () => {
		expect(orderSlice(undefined, {type:undefined})).toEqual({
			count:{},
			currentPost: {},
			isLoading: true,
			isModal: false
		})
	})
    it("actionLoading", () => {
		const previousState = {
			isLoading:  true,
		}
		expect(orderSlice(previousState, actionLoading({"article":"01"}))).toEqual({
			isLoading:  {"article":"01"},
		})
	})
	it("actionModal", () => {
		const previousState = {
			isModal: false
		}
		expect(orderSlice(previousState, actionOrderIsModal())).toEqual({
			isModal: true,
		})
	})

	it("actionAddCurrentPost", () => {
		const previousState = {
			currentPost:{}
		}
		expect(orderSlice(previousState, actionCurrentPost({"article":"01"}))).toEqual({
			currentPost: {"article":"01"},
		})
	})

	it("actionOrders", () => {
		const previousState = {
			count:
				{['1']:1,['2']:2}
			
		}
		expect(orderSlice(previousState, actionOrder({'article':5}))).toEqual({
			count: 
				{['1']:1,['2']:2,['5']:1}
			,
		})
		expect(orderSlice(previousState, actionPlus({'article':1}))).toEqual({
			count: 
				{['1']:2,['2']:2}
			,
		})
		expect(orderSlice(previousState, actionMinus({'article':2}))).toEqual({
			count: 
				{['1']:1,['2']:1}
			,
		})
	})
})


