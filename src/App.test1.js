import React from "react";
import {render} from '@testing-library/react'
import App from './App.jsx'
import TodoProvider from './context/context.jsx'

describe('App', () => {
	it("snapshot APP", () => {
		const app = render(<TodoProvider><App/></TodoProvider>)
		expect(app).toMatchSnapshot()
	})
})
