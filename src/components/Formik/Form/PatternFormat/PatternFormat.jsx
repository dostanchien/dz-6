import {ErrorMessage} from "formik"
import {PatternFormat} from 'react-number-format';
import PropTypes from 'prop-types';
import cn from 'classnames'
import '../InputForm.scss'

const PatternFormatBox = (props) => {
	const {
		format,
		className,
		label,
		type,
		name,
		mask,
		error,
		value,
		...restProps
	} = props
	return (
		<label className={cn('form-item', className, {'has-validation': error})}>
			<p className="form-label">{label}</p>
						<PatternFormat format={format} mask={mask} value={value} name={name} {...restProps}/>	
						<ErrorMessage className="error-message" name={name} component='p'/>
		</label>
	)
}


PatternFormatBox.defaultProps = {
	mask:'',
	value:''
}

PatternFormatBox.propTypes = {
	className: PropTypes.string,
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	mask: PropTypes.string,
	value: PropTypes.string,
	error: PropTypes.object
}

export default PatternFormatBox
