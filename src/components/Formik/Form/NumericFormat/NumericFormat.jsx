import {ErrorMessage} from "formik"
import {NumericFormat} from 'react-number-format';
import PropTypes from 'prop-types';
import cn from 'classnames'
import '../InputForm.scss'

const NumericFormatBox = (props) => {
	const {
		className,
		label,
		name,
		placeholder,
		error,
		isAllowed,
		...restProps
	} = props
	return (
		<label className={cn('form-item', className, {'has-validation': error})}>
			<p className="form-label">{label}</p>
			<NumericFormat placeholder={placeholder} isAllowed={isAllowed}  name={name} {...restProps}/>
			<ErrorMessage className="error-message" name={name} component='p'/>
		</label>
	)
}


NumericFormatBox.defaultProps = {
    isAllowed: () => {},
}

NumericFormatBox.propTypes = {
	className: PropTypes.string,
	label: PropTypes.string.isRequired,
	type: PropTypes.string,
	name: PropTypes.string.isRequired,
	placeholder: PropTypes.string,
	isAllowed: PropTypes.func,
	error: PropTypes.object
}

export default NumericFormatBox
