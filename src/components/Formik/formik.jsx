import React from 'react';
import { Formik, Form} from 'formik';
import {useDispatch, useSelector} from "react-redux";
import InputBox from "./Form/Input/Input.jsx";
import TextAreaBox from "./Form/TextArea/TextArea.jsx";
import Button from "../Button/Button.jsx";
import PropTypes from 'prop-types';
import {actionDeleteCooki} from "../../store/orderSlice.js";

import {selectorOrderCount} from "../../store/selectors.js";
import {validationSchema} from "../Formik/Form/validation.js";

import '../Formik/Form/PageForm.scss'


const PageForm = ({messagesOrder}) => {
const dispatch = useDispatch()
	
	const orderCount = useSelector(selectorOrderCount);
	return (
	<div className="formik-conteiner">
		<div className="page__dashboard" >
			<div className="page">
            <Formik 
					initialValues={{ yuname: "", surname: "", year: "", delivery: "", phone: "+380",messOrder:messagesOrder}}
					validationSchema={validationSchema}
					onSubmit={(values, {resetForm}) => {
						let mess="Ваше замовлення оформлене.\n";
						 mess+="Ім'я: "+values.yuname+"\n"+"Прізвище: "+values.surname+"\n"+"Вік: "+values.year+"\n"+"Адреса: "+values.delivery+"\n"+"Телефон: "+values.phone+"\n";
						mess+="\n"+values.messOrder+"\n";
						 for (let key in orderCount)
						 	mess+=orderCount[key]+messagesOrder[key]
						 mess+="Наш менеджер зв'яжеться з Вами"
						 console.log(mess,values.phone.replaceAll(" ", ""))
						 alert(mess)
						dispatch(actionDeleteCooki())
						resetForm()
					}}>
					{({errors, touched}) => {
						return (
							<Form>
								<fieldset className="form-block">
									<legend>Форма замовлення</legend>
									<div className="row">
										<div className="col">

											<InputBox
												className="mb-3"
												label="Ім'я"
												name="yuname"
												placeholder="Ім'я"
												error={errors.yuname && touched.yuname}
											/>
											<InputBox
												className="mb-3"
												label="Прізвище"
												name="surname"
												placeholder="Прізвище"
												error={errors.surname && touched.surname}
											/>
											<InputBox
                                                className="mb-3"
                                                label="Вік"
                                                name="year"
                                                placeholder="XX" 
                                               error={errors.year && touched.year}
										/>
                                            <TextAreaBox
                                                name={"delivery"}
                                                label={"Адреса"}
                                                placeholder="Адреса доставки"
                                                error={errors.delivery && touched.delivery}
                                            />  
											<InputBox
                                                className="mb-3"
												name={"phone"}
												label={"Телефон"}
                                                placeholder="+380 XX XXX-XX-XX"
												error={errors.phone && touched.phone}
										/>
										                                           
										</div>
									</div>
								</fieldset>


								<div className="col-12">
									<Button type="submit">Відправити</Button>
								</div>
							</Form>
						)
					}}
				</Formik>
			</div>
		</div>
    </div>
	);
};
PageForm.propTypes = {
	messagesOrder: PropTypes.array

}
export default PageForm;
