import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { FormItem} from "formik-antd";
import { NumericFormat, PatternFormat} from 'react-number-format';
import {useDispatch, useSelector} from "react-redux";
import InputBox from "./Form/Input/Input.jsx";
import TextAreaBox from "./Form/TextArea/TextArea.jsx";
import NumericFormatBox from "./Form/NumericFormat/NumericFormat.jsx";
import PatternFormatBox from "./Form/PatternFormat/PatternFormat.jsx";
import Button from "../Button/Button.jsx";


import {selectFromData} from "../../store/selectors.js";
import {actionUpdateCv} from "../../store/actions.js";
import {validationSchema} from "../Formik/Form/validation.js";

import '../Formik/Form/PageForm.scss'


const PageForm = () => {
	//const formData = useSelector(selectFromData)
	//const dispatch = useDispatch()

	//console.log('formData', formData);

    const MAX_YAR = 100;
	return (
	<div className="formik-conteiner">
		<div className="page__dashboard" >
			<div className="page">
            <Formik
					initialValues={{ yuname: "", surname: "", year: "", delivery: "", phone: "",}}
					validationSchema={validationSchema}
					onSubmit={(values, {resetForm}) => {
						console.log('values', values);
						//dispatch(actionUpdateCv(values))
						resetForm()
					}}>
					{({errors, touched}) => {
						console.log('errors', errors)
						console.log("touched", touched)
						return (
							<Form>
								<fieldset className="form-block">
									<legend>Форма замовлення</legend>
									<div className="row">
										<div className="col">

											<InputBox
												className="mb-3"
												label="Ім'я"
												name="yuname"
												placeholder="Ім'я"
												error={errors.yuname && touched.yuname}
											/>
											<InputBox
												className="mb-3"
												label="Прізвище"
												name="surname"
												placeholder="Прізвище"
												error={errors.surname && touched.surname}
											/>
                                            <NumericFormatBox
                                                className="mb-3"
                                                label="Вік"
                                                name="year"
                                                placeholder="XX" 
                                               error={errors.year && touched.year}
                                                isAllowed={(values) => {
                                                    if (!values.value) return true;
                                                    const { floatValue } = values;
                                                    console.log("floatValue",floatValue)
                                                    return floatValue < MAX_YAR;
                                                }}
                                            />
                                            <TextAreaBox
                                                name={"delivery"}
                                                label={"Адреса"}
                                                placeholder="Адреса доставки"
                                                error={errors.delivery && touched.delivery}
                                            />  
                                            <PatternFormatBox
                                            format="+38 (0##) ###-##-##" 
                                            allowEmptyFormatting mask="_" 
                                            name={"phone"}
                                            label={"Телефон"}
                                            className={"mb-3"}
											//autocomplete="on"
                                            error={errors.phone && touched.phone}
											isAllowed={(values) => {
												if (!values.value) return true;
												const { floatValue } = values;
												console.log("floatValue",focus.blur)
												return floatValue;
											}}
                                            />                                                      
										</div>
									</div>
								</fieldset>



								<div className="col-12">
									<Button type="submit">Відправити</Button>
								</div>
							</Form>
						)
					}}
				</Formik>
			</div>
		</div>
    </div>
	);
};

export default PageForm;
