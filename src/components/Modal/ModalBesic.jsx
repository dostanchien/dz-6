import React from "react";
import PropTypes from 'prop-types'
import ModalWrapper from "./ModalWrapper"
import Modal from "./Modal"
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalClose from "./ModalClose"


const ModalBase = ({title, desc, handleOk, handleClose, isOpen}) =>{

    const handleOutside = (event) => {
        if(!event.target.closest(".modal")){
            handleClose()
        }
    }

    return(
        <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
            <Modal>
                <ModalHeader>
                   <ModalClose click={handleClose}/>
                </ModalHeader>
                <ModalBody>
                    <h4>{title}</h4>
                    <p>{desc}</p>
                </ModalBody>
                <ModalFooter textFirst="OK" textSecondary="Close" clickFirst={handleOk} clickSecondary={handleClose}/>
            </Modal>
        </ModalWrapper>
    )
}

ModalBase.propTypes = {
    title: PropTypes.string,
    desc: PropTypes.string,
    handleOk: PropTypes.func,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}



export default ModalBase
