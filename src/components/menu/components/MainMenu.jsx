/*Навбар
Остался последний компонент, навбар. О функции useOnClickOutside будет идти речь ниже, а пока всё оставим как есть.

//import {ReactComponent as Favorite} from '../../../icons/favorite.svg?react';
src/components/MainMenu.js */

import React, { useRef, useContext } from 'react';
import {useEffect} from  "react";
import styled from 'styled-components';
import useOnClickOutside from '../hooks/onClickOutside';
import { MenuContext } from '../context/navState.jsx';
import HamburgerButton from './HamburgerButton.jsx';
import { SideMenu } from './SideMenu.jsx';
import {Link} from "react-router-dom";
import './Favorite.scss';
import Favorite from "../../../icons/favorite.svg?react";
import Cart from '../../../icons/cart.svg?react';
import BoxMenu from './BoxMenu';
import {useDispatch,useSelector} from "react-redux";
import {selectorFavorite,selectorOrderCount} from "../../../store/selectors.js";
import {actionOrderCooki} from "../../../store/orderSlice.js";
import {actionFavoriteCooki} from "../../../store/favoriteSlice.js";

const Navbar = styled.div`
  display: flex;
  position: fixed;
  left: 0;
  right: 0;
  box-sizing: border-box;
  outline: currentcolor none medium;
  max-width: 100%;
  margin: 0px;
  align-items: center;
  background: #082bff none repeat scroll 0% 0%;
  color: rgb(248, 248, 248);
  min-width: 0px;
  min-height: 0px;
  flex-direction: row;
  justify-content: flex-start;
  padding: 6px 60px;
  box-shadow: rgba(0, 0, 0, 0.2) 0px 8px 16px;
  z-index: 500;
`;

const MainMenu = () => {
  const node = useRef();
  const { isMenuOpen, toggleMenuMode } = useContext(MenuContext);
  useOnClickOutside(node, () => {
    // Only if menu is open
    if (isMenuOpen) {
      toggleMenuMode();
    }
  });
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actionOrderCooki());
    dispatch(actionFavoriteCooki());
  }, [])
  return (
    <header ref={node}>
      <Navbar>
        <HamburgerButton />
        <Link to="/" className='text-white'><h1>Website</h1></Link>
        <div className="wraper-box-menu">
        <BoxMenu 
            title={"Список обраних"}
            count={Object.keys(useSelector(selectorFavorite)).length||0}
            url="favorite"
            >
            <Favorite/> 
          </BoxMenu>
          <BoxMenu      
            title={"Кошик товарів"}
            count={Object.keys(useSelector(selectorOrderCount)).length||0} 
            url="order"
          >
            <Cart/>  
          </BoxMenu>
        </div>
      </Navbar>
      <SideMenu />
    </header>
  );
};
export default MainMenu;