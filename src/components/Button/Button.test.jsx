import React from "react";
import Button from "./Button.jsx";
import { vi, describe, expect} from 'vitest';
import {render,screen,fireEvent} from "@testing-library/react";

const handelClick =  vi.fn()
describe("test Button", ()=>{
    test('isButton', () => {
        render(<Button>text</Button>)
         expect(screen.getByText("text")).toBeInTheDocument()
    })
    test ('isProps',()=>{
        render(<Button className={"btn-primari"}>text</Button>)
	    expect(screen.getByText("text")).toHaveClass("btn-primari")
    })
    test ('isType',()=>{
        render(<Button>text</Button>)
	    expect(screen.getByText("text")).toHaveAttribute("type","button")
    })

    test ('change',()=>{
        render(<Button type={"submite"}>text</Button>)
	    expect(screen.getByText("text")).toHaveAttribute("type","submite")
    })

    test ('link',()=>{
      const {container} = render(<Button href={"/link"}>text</Button>);
      const link = container.querySelector("a");
	    expect(link).toBeInTheDocument()
    })
    test ('onClick',()=>{
        const {container} = render(<Button onClick={handelClick}>text</Button>)
        const button = container.firstChild
        fireEvent.click(button)
	    expect(handelClick).toHaveBeenCalled()
    })

})
