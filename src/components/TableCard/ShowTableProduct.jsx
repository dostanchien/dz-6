import PropTypes from 'prop-types'
const ShowTableProduct = ({productsItems}) =>{
    return(
        <div className="table-responsive">
            <table className="table table-bordered table-hover">
                <thead className='thead-dark'>
                    <tr>
                        <th>Артикул</th>
                        <th>Фото</th>
                        <th>Назва</th>
                        <th>Цвет</th>
                        <th>Цена</th>
                        <th>Купить</th>
                    </tr>
                </thead>
                <tbody>
                    {productsItems}
                </tbody>
            </table>
        </div>
    )
}
ShowTableProduct.propTypes = {
    productsItems: PropTypes.any
}
export default ShowTableProduct