import PropTypes from 'prop-types'
import styled from "styled-components"
import CardFooter from '../Card/Components/CardFooter';
import Favorite from  '../../icons/favorite.svg?react';
const CardSt=styled.td`
position: relative;
display: -ms-flexbox;
display: flex;
-ms-flex-direction: column;
flex-direction: column;
min-width: 0;
word-wrap: break-word;
background-color: #fff;
background-clip: border-box;
border: 1px solid rgba(0, 0, 0, 0.125);
border-radius: 0.25rem;
margin-bottom: 1rem;
-ms-flex: 1 0 0%;
flex: 1 0 0%;
margin-right: 1rem;
margin-bottom: 0;
margin-left: 1rem;
float: left;
width:7rem
`;
const ImgSt =styled.img`
  -ms-flex-negative: 0;
  flex-shrink: 0;
  width: 100%;
  `;
  const SmallSt =styled.td`
  font-size:70%;
  text-align: left;
  font-weight:50;
  `;
  const PriceSt =styled.td`
  text-align: center;
  font-weight:400;
  color:  darkgreen;
  `;
  const ContainerSt= styled.span` 
    position: absolute;
    top: 15%;
    left: 85%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    background-color: #555;
    font-size: 1rem;
    padding: 0.8rem;
    border: none;
    cursor: pointer;
    border-radius: 1rem;
    opacity: 0.6;
    &:hover, svg:hover {
        background-color: black;}
    svg {
        position: absolute;
        top:50%;
        fill:${props => props.fill};
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        opacity: 1;
    }
  `;
const RowProduct = ({products,colorFavorite,clickFirst,onClickIcon}) =>{
    return(
        <tr>
            <SmallSt>{products.article}</SmallSt>
            <CardSt>
                    <figure>              
                        <ImgSt src={products.images} alt={products.name} />
                        <ContainerSt 
                                    fill={colorFavorite}
                                    onClick={onClickIcon}
                        >
                            <Favorite/>
                        </ContainerSt>
                        
                    </figure>
            </CardSt>
            <td>{products.name}</td>
            <td>{products.color}</td>
            <PriceSt>{products.price} грн.</PriceSt>
            <td><CardFooter className='small' textFirst="Купить" clickFirst={clickFirst}/></td>
        </tr>
    )
}
RowProduct.defaultProps = {
	clickFirst: () => {},
    onClickIcon: () => {},
  }
RowProduct.propTypes = {
    onClickIcon: PropTypes.func,
	colorFavorite: PropTypes.string,
	products: PropTypes.object,
    clickFirst: PropTypes.func

}

export default RowProduct