import React from "react";
import CardBody from './CardBody.jsx';
import { describe, expect} from 'vitest';
import {render,screen} from "@testing-library/react";

describe("test CardBody", ()=>{
	
    test('Children', () => {
        render(<CardBody>text</CardBody>)
         expect(screen.queryByText("text")).toBeInTheDocument()
    })
})


