import React from "react";
import CardHeader from './CardHeader.jsx';
import { describe, expect} from 'vitest';
import {render,screen} from "@testing-library/react";

describe("test CardHeader", ()=>{
	
    test('Children', () => {
        render(<CardHeader>text</CardHeader>)
         expect(screen.queryByText("text")).toBeInTheDocument()
    })
})


