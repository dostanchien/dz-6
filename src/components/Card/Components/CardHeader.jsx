import PropTypes from 'prop-types'
import styled from 'styled-components'
const HeaderSt = styled.div`   
  padding: 0.75rem 1.25rem;
  margin-bottom: 0;
  background-color: rgba(0, 0, 0, 0.03);
  border-bottom: 1px solid rgba(0, 0, 0, 0.125);
`;
const CardHeader = ({children})=>{
    return(
        <HeaderSt>{children}</HeaderSt>
    )
}

CardHeader.propTypes = {
    children: PropTypes.any
}

export default CardHeader