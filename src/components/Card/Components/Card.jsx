import PropTypes from 'prop-types'
import styled from "styled-components"
import CardHeader from './CardHeader';
import CardAjax from './CardAjax';
import CardBody from './CardBody';
import CardFooter from './CardFooter';
import Favorite from '../../../icons/favorite.svg?react';

const CardSt=styled.div`
position: relative;
display: -ms-flexbox;
display: flex;
-ms-flex-direction: column;
flex-direction: column;
min-width: 0;
word-wrap: break-word;
background-color: #fff;
background-clip: border-box;
border: 1px solid rgba(0, 0, 0, 0.125);
border-radius: 0.25rem;
margin-bottom: 1rem;
-ms-flex: 1 0 0%;
flex: 1 0 0%;
margin-right: 1rem;
margin-bottom: 0;
margin-left: 1rem;
float: left;
width:15rem
`;
const ImgSt =styled.img`
  -ms-flex-negative: 0;
  flex-shrink: 0;
  width: 100%;
  `;
  const SmallSt =styled.p`
  font-size:70%;
  text-align: right;
  font-weight:50;
  `;
  const PriceSt =styled.p`
  font-size:150%;
  text-align: center;
  font-weight:400;
  color:  darkgreen;
  padding-top: 1rem;
  `;
  const ContainerSt= styled.div` 
    position: absolute;
    top 55%;
    left: 90%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    background-color: #555;
    font-size: 1rem;
    padding: 1rem;
    border: none;
    cursor: pointer;
    border-radius: 1rem;
    opacity: 0.6;
    &:hover, svg:hover {
        background-color: black;}
    svg {
        position: absolute;
        top:50%;
        fill:${props => props.fill};
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        opacity: 1;
    }
  `;
  
const Card = ({products,colorFavorite,clickFirst,onClickIcon})=>{
    return(
            <CardSt>
               <CardHeader>
                    <figure>              
                        <ImgSt src={products.images} alt={products.name} />
                        <ContainerSt 
                                    fill={colorFavorite}
                                    onClick={onClickIcon}
                        >
                            <Favorite/>
                        </ContainerSt>
                        
                    </figure>
                    <CardAjax>{products.color}</CardAjax>
                </CardHeader>
                <CardBody>
                    <SmallSt>Артикул: {products.article}</SmallSt>
                    <h4 className='text-center'>{products.name}</h4>            
                    <PriceSt>{products.price} грн.</PriceSt>
                </CardBody>
  
                <CardFooter textFirst="Купить" clickFirst={clickFirst}/>
            </CardSt>
    )
}

Card.defaultProps = {
	clickFirst: () => {},
    onClickIcon: () => {},
  }
Card.propTypes = {
    onClickIcon: PropTypes.func,
	colorFavorite: PropTypes.string,
	products: PropTypes.object,
    clickFirst: PropTypes.func

}

export default Card