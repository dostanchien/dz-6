import React from 'react';
import styled from "styled-components"
import TableSvg from "../../icons/table.svg?react";
import CardSvg from "../../icons/card.svg?react";
import { useContext } from 'react';
import { MyContext } from "../../Context/MyContext.js";


const FlexContainerSt =styled.div`
display: flex;
justify-content: center;
background:linear-gradient(160deg, #f6f4f6, #d2d1d2);
  
div {
    width: 32px;
    margin: 5px;
    text-align: center;
    border-radius: 6px;
    border: 1px outset #888;
    position: relative;
    display: inline-block;
    padding: 5px;
    text-transform: uppercase;
    letter-spacing: 4px;
    overflow: hidden;
    
    box-shadow: 0 0 10px rgb(235, 234, 234);
    font-family: verdana;
    font-weight: bolder;
    text-decoration: none;
    background:linear-gradient(160deg, #fcfafa, #b4b3b3);
    text-shadow: 0px 0px 2px rgba(0, 0, 0, .5);
  
    transition: 0.2s;
  }
div:hover{
    fill:green;
    border-color: #EBEBEB;
    cursor:pointer;
    transform: scale(1.1);
  }
  `;
const DivCardSt =styled.div`
    fill:${props => props.fill};
  `;
const DivTableSt =styled.div`
    fill:${props => props.fill};
    `;
    
const CheckList = () =>{
    const {isCheck, setCheck} = useContext(MyContext);
    return(
    <>
        <FlexContainerSt>
            <DivCardSt  fill= {isCheck ? "green": "black"}
                        onClick={() => {setCheck(true); localStorage.setItem("isCheck", JSON.stringify(true))}}
                        >
              <CardSvg/>
            </DivCardSt>
            <DivTableSt  fill={!isCheck ? "green": "black"} 
                         onClick={() => {setCheck(false); localStorage.setItem("isCheck", JSON.stringify(false))}}
                         >
              <TableSvg/>
            </DivTableSt>
        </FlexContainerSt></>
    )
}
export default CheckList

