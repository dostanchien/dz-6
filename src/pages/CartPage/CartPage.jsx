import CardOrder from '../../components/Card/Components/CardOrder.jsx';
import {useState,useEffect} from  "react";
import {useDispatch,useSelector} from "react-redux";
import {selectorFavorite,selectorOrderCount,selectorOrderIsModal,selectorProducts} from "../../store/selectors.js";
import {actionFetchProducts} from "../../store/productsSlice.js";
import {actionFavorite} from "../../store/favoriteSlice.js";
import {actionOrder,actionOrderIsModal,actionOrderCooki,actionDeleteCooki} from "../../store/orderSlice.js";
import ModalImage from "../../components/Modal/ModalImage/ModalImage.jsx";
import CardFooter from '../../components/Card/Components/CardFooter.jsx';
import CardBody from '../../components/Card/Components/CardBody.jsx';
import styled from "styled-components";
import PageForm from "../../components/Formik/formik.jsx";
const CartPage = () => {

const date = useSelector(selectorProducts);
const favorite = useSelector(selectorFavorite);
const isOpenOrder=useSelector(selectorOrderIsModal);
const orderCount = useSelector(selectorOrderCount);
const dispatch = useDispatch();
useEffect(() => {
  dispatch(actionFetchProducts(date));
  dispatch(actionOrderCooki(orderCount));
}, [])
const [currentPost, setCurrentPost] = useState({});
const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)

const handelModal = (is) =>       dispatch(actionOrderIsModal(is))
const handleFavorites = (item) => dispatch(actionFavorite(item))
const handleOrder = (item) =>     dispatch(actionOrder(item))
let sum=0;
let number=0;
let messagesOrder=[];
let i=0;
	const productsItems = date.map((item,index) => 
  { 
    if (orderCount[item.article]){
      sum+=item.price*orderCount[item.article];
      number+=orderCount[item.article];
      ++i;
     messagesOrder[item.article]=" од. "+item.name+" - "+item.color+". Артикул: " +item.article+"\n";
    return( 
          <CardOrder   products={item}  key={item.article}
                  colorFavorite={!favorite[item.article] ? "white" : "red"}//меняем цвета иконок фаворите=проверка
                  onClickIcon={()=>handleFavorites(item)}
                  onClickClose={()=>{handelModal(), handleCurrentPost(item)}}
          />
          )
    }
  })//end date.map
  return(    
<>
      <h2>Кошик</h2>
	{Object.keys(orderCount).length==0
    ? <>
        <div className="text-center">
            <img src="https://diplomus.kiev.ua/images/cart_null.gif" alt="Корзина пуста"/>
        </div>
      </>
		 : <><SectionSt>
                  {productsItems}
        </SectionSt></>}
        <SectionSt hidden={Object.keys(orderCount).length==0 ? true : '' }>
                     <CardBody>
                      <SumaSt>Вибрано {Object.keys(useSelector(selectorOrderCount)).length} товарів у кількості {number} <span>од.</span> у сумі {sum} <span>грн.</span></SumaSt>
                      </CardBody>
                     <CardFooter textFirst="Очистити кошик" clickFirst={()=>{dispatch(actionDeleteCooki(orderCount))}}/>
        </SectionSt>
        <SectionSt  hidden={Object.keys(orderCount).length==0 ? true : '' }>
                    <PageForm messagesOrder={messagesOrder}/>
        </SectionSt>
     
	 <ModalImage
              isOpen={isOpenOrder}
              handleClose={()=>handelModal()}
              products={currentPost}
              handleOk={()=> {handelModal(), handleOrder(currentPost)}}
    />
</>
  ) 
}
export default CartPage

const SectionSt=styled.div`
display: flex-row;
-ms-flex-direction: row;
flex-direction: row;
background-clip: border-box;
border: 1px solid rgba(0, 0, 0, 0.125);
border-radius: 0.25rem;
margin-bottom: 1rem;
-ms-flex: 1 0 0%;
flex: 1 0 0%;
margin:1rem;
float: left;
width:100%;
`;
const SumaSt=styled.p`
font-size:130%;
color:#2899C0;
font-weight:600;

span{
font-size:80%;
}
`;
