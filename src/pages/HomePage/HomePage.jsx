import Card from '../../components/Card/Components/Card.jsx';
import {useState,useEffect} from  "react";
import {MyContext } from "../../Context/MyContext.js";
import {useDispatch,useSelector} from "react-redux";
import {actionFetchProducts} from "../../store/productsSlice.js";
import {selectorProducts,selectorFavorite,selectorOrderIsModal} from "../../store/selectors.js";
import { actionFavorite } from "../../store/favoriteSlice.js";
import { actionOrder,actionOrderIsModal} from "../../store/orderSlice.js";
import ModalImage from "../../components/Modal/ModalImage/ModalImage.jsx";
import ShowTableProduct from '../../components/TableCard/ShowTableProduct.jsx';
import RowProduct from '../../components/TableCard/RowProduct.jsx';
import CheckList from '../../components/ChekList/ChekList.jsx';

const HomePage = () => {
const [isCheck, setCheck] = useState();


const date = useSelector(selectorProducts);
const favorite = useSelector(selectorFavorite);
const isOpenOrder=useSelector(selectorOrderIsModal);
const dispatch = useDispatch();
useEffect(() => {
  dispatch(actionFetchProducts(date));
  const u=JSON.parse(localStorage.getItem("isCheck"))
  u!=null ? setCheck(u) : setCheck(true);
  
}, [])

const [currentPost, setCurrentPost] = useState({});
const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)

const handelModal = (is) =>       dispatch(actionOrderIsModal(is))
const handleFavorites = (item) => dispatch(actionFavorite(item))
const handleOrder = (item) =>     dispatch(actionOrder(item))


	const productsItemsCard = date.map((item, index) => {
  return( 
    <Card   products={item}  key={item.article}
                  colorFavorite={!favorite[item.article] ? "white" : "red"}//меняем цвета иконок фаворите=проверка
                  onClickIcon={()=>handleFavorites(item)}
                  clickFirst={()=>{handelModal(), handleCurrentPost(item)}}
          />
  )
  })//end date.map
  const productsItemsTbl = date.map((item, index) => {
    return( 
    <RowProduct   products={item}  key={item.article}
                  colorFavorite={!favorite[item.article] ? "white" : "red"}//меняем цвета иконок фаворите=проверка
                  onClickIcon={()=>handleFavorites(item)}
                  clickFirst={()=>{handelModal(), handleCurrentPost(item)}}
    />)
    })//end date.map

  return(    
<>
      <h2>Керамічні обігрівачі</h2>

      <MyContext.Provider value={{isCheck, setCheck}}>
          <CheckList/>
      </MyContext.Provider>
    {isCheck ?
              productsItemsCard    
              :
              <ShowTableProduct productsItems={productsItemsTbl}/>
}
	 <ModalImage
              isOpen={isOpenOrder}
              handleClose={()=>handelModal()}
              products={currentPost}
              handleOk={()=> {handelModal(), handleOrder(currentPost)}}
    />
</>
  ) 
}
export default HomePage
