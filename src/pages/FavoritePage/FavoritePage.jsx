import Card from '../../components/Card/Components/Card.jsx';
import {useState,useEffect} from  "react";
import {useDispatch,useSelector} from "react-redux";
import {selectorFavorite,selectorOrderIsModal,selectorProducts} from "../../store/selectors.js";
import {actionFetchProducts} from "../../store/productsSlice.js";
import {actionFavorite,actionFavoriteCooki} from "../../store/favoriteSlice.js";
import {actionOrder,actionOrderIsModal} from "../../store/orderSlice.js";
import ModalImage from "../../components/Modal/ModalImage/ModalImage.jsx";


const FavoritePage = () => {

const date = useSelector(selectorProducts);
const favorite = useSelector(selectorFavorite);
const isOpenOrder=useSelector(selectorOrderIsModal);
const dispatch = useDispatch();

useEffect(() => {
  dispatch(actionFetchProducts(date));
  dispatch(actionFavoriteCooki(favorite));
}, [])
const [currentPost, setCurrentPost] = useState({});
const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)

const handelModal = (is) =>       dispatch(actionOrderIsModal(is))
const handleFavorites = (item) => dispatch(actionFavorite(item))
const handleOrder = (item) =>     dispatch(actionOrder(item))

	const productsItems = date.map((item, index) => 
{
  if (favorite[item.article]){
  return( 
  <Card   products={item}  key={item.article}
          colorFavorite={"red"}
          onClickIcon={()=>handleFavorites(item)}
          clickFirst={()=>{handelModal(), handleCurrentPost(item)}}
  />
  )
  }
  })//end date.map
  return(    
<>
      <h2>Список обраних</h2>
	{favorite.length==0 ? <>
		<div className="text-center">
				<img src="https://i.pinimg.com/474x/38/43/e9/3843e9698cd78aa52d657301932318bc.jpg" alt="Список обраних пуст"/>
		</div></>
		 : productsItems}
	 <ModalImage
              isOpen={isOpenOrder}
              handleClose={()=>handelModal()}
              products={currentPost}
              handleOk={()=> {handelModal(), handleOrder(currentPost)}}
    />
</>
  ) 
}
export default FavoritePage
